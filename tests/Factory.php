<?php

namespace Tests\GiiftCore\UltraVoucher;

use Giift\Cart\CartItem;
use Giift\Cart\CartOrderItem;
use Giift\Model\Service\ICard;
use Giift\Model\Service\IDmo;
use Giift\UltraVoucher\Api;
use Ramsey\Uuid\Uuid;

class Factory extends \PHPUnit\Framework\TestCase
{
    /** @var \GiiftCore\UltraVoucher\Factory */
    private $factory;

    protected function setUp()
    {
        parent::setUp();
        $this->factory = new \GiiftCore\UltraVoucher\Factory;
    }

    public function tearDown()
    {
        parent::tearDown();
        \Mockery::close();
    }

    /**
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function testGetBalance()
    {
        $body = [
            'http_code' => 200,
            'status' => 'success',
            'code' => 'SSR001',
            'message' => 'Balance is available',
            'data' => [
                'balance' => '48,824,000',
                'date' => '2018-11-19 04:23:54',
            ],
        ];
        $mockApi = \Mockery::mock(Api::class);
        $mockApi->shouldReceive('getBalanceDeposit')->once()->andReturn($body);
        $this->factory->setUltraVoucherApi($mockApi);

        static::assertEquals(48824000, $this->factory->getBalance());
    }

    /**
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function testFailingGetBalance()
    {
        $body = [
            'http_code' => 200,
            'status' => 'success',
            'code' => 'SSR001',
            'message' => 'Balance is available',
        ];
        $mockApi = \Mockery::mock(Api::class);
        $mockApi->shouldReceive('getBalanceDeposit')->once()->andReturn($body);
        $this->factory->setUltraVoucherApi($mockApi);

        try {
            $this->factory->getBalance();
            static::assertFalse(true, 'an exception should have been thrown');
        } catch (\Exception $ex) {
            static::assertStringStartsWith('Could not fetch balance ', $ex->getMessage());
        }
    }

    public function testGetBalanceCurrency()
    {
        static::assertEquals('IDR', $this->factory->getBalanceCurrency());
    }

    /**
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function testCreate()
    {
        $pimple = new \Pimple\Container;
        $dmoId = Uuid::uuid1()->toString();
        $pimple[IDmo::class] = function () use ($dmoId) {
            $dmo = \Mockery::mock(\Giift\Model\IDmo::class);
            $dmo->shouldReceive('getExternalReference')->once()->andReturn('SKU-UV-00050');
            $dmoService = \Mockery::mock(IDmo::class);
            $dmoService->shouldReceive('getFromId')->withArgs([$dmoId])->andReturn($dmo);

            return $dmoService;
        };
        $pimple[ICard::class] = function () {
            $cardService = \Mockery::mock(ICard::class);
            $card = \Mockery::mock(\Giift\Model\ICard::class);
            $card->shouldReceive('setDmo')->once()->andReturn($card);
            $card->shouldReceive('setNumber')->once()->withArgs(['H8reOS2l'])->andReturn($card);
            $card->shouldReceive('setExpiry')->once()->andReturn($card);
            $card->shouldReceive('setValue')->once()->withArgs([42])->andReturn($card);
            $card->shouldReceive('setStatus')->once()->andReturn($card);
            $card->shouldReceive('setExtraLogin')->once()->andReturn($card);
            $cardService->shouldReceive('getNew')->once()->andReturn($card);
            $cardService->shouldReceive('save')->once()->andReturn($card);
            return $cardService;
        };
        $this->factory->setContainer(new \Pimple\Psr11\Container($pimple));

        $mockApi = \Mockery::mock(Api::class);

        $openOrder = [
            "http_code" => 200,
            "status" => "success",
            "code" => "SSROO1",
            "message" => "Order successfully created",
            "data" => [
                "order_no" => Uuid::uuid1()->toString(),
                "invoice_no" => "GII-20181115-000011"
            ]
        ];
        $mockApi->shouldReceive('openOrder')->once()->andReturn($openOrder);

        $getOrder = [
            'http_code' => 200,
            'status' => 'success',
            'code' => 'SSR001',
            'message' => 'Data has been sent',
            'data' => [
                [
                    'id_voucher' => 'VC50-20181005-0072',
                    'kode_voucher' => 'SDhyZU9TMmw=',
                    'tgl_expired' => '2019-05-30',
                    "url" => "https://uvcr.me/f39b9a51250556be8924b85da9558e6b/1537176870"]
            ]
        ];
        $mockApi->shouldReceive('getDataOrder')->once()->andReturn($getOrder);

        $this->factory->setUltraVoucherApi($mockApi);

        $orderItem = new CartOrderItem();
        $cartItem = new CartItem();
        $orderItem->setCartItem($cartItem)
            ->setId(Uuid::uuid1()->toString());
        $cartItem->setBuyableType('card')
            ->setBuyableId($dmoId)
            ->setQuantity(42);
        $result = $this->factory->create($orderItem);
        static::assertTrue($result->complete);
        static::assertInstanceOf(\Giift\Model\ICard::class, $result->data);
    }

    /**
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function testCreateWithBadDmoId()
    {
        $pimple = new \Pimple\Container;
        $dmoId = Uuid::uuid1()->toString();
        $pimple[IDmo::class] = function () use ($dmoId) {
            $dmoService = \Mockery::mock(IDmo::class);
            $dmoService->shouldReceive('getFromId')->withArgs([$dmoId])->andReturn(null);

            return $dmoService;
        };
        $this->factory->setContainer(new \Pimple\Psr11\Container($pimple));

        $orderItem = new CartOrderItem();
        $cartItem = new CartItem();
        $orderItem->setCartItem($cartItem)
            ->setId(Uuid::uuid1()->toString());
        $cartItem->setBuyableType('card')
            ->setBuyableId($dmoId)
            ->setQuantity(42);
        try {
            $this->factory->create($orderItem);
            static::assertFalse(true, 'an exception should have been thrown');
        } catch (\Exception $ex) {
            static::assertStringStartsWith('Invalid dmo Id #', $ex->getMessage());
        }
    }

    /**
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function testCreateWithNoSku()
    {
        $pimple = new \Pimple\Container;
        $dmoId = Uuid::uuid1()->toString();
        $pimple[IDmo::class] = function () use ($dmoId) {
            $dmo = \Mockery::mock(\Giift\Model\IDmo::class);
            $dmo->shouldReceive('getExternalReference')->once()->andReturn(null);
            $dmoService = \Mockery::mock(IDmo::class);
            $dmoService->shouldReceive('getFromId')->withArgs([$dmoId])->andReturn($dmo);

            return $dmoService;
        };
        $this->factory->setContainer(new \Pimple\Psr11\Container($pimple));

        $orderItem = new CartOrderItem();
        $cartItem = new CartItem();
        $orderItem->setCartItem($cartItem)
            ->setId(Uuid::uuid1()->toString());
        $cartItem->setBuyableType('card')
            ->setBuyableId($dmoId)
            ->setQuantity(42);
        try {
            $this->factory->create($orderItem);
            static::assertFalse(true, 'an exception should have been thrown');
        } catch (\Exception $ex) {
            static::assertStringEndsWith(
            //@codingStandardsIgnoreLine
                'cannot be bought with ultra-voucher as it does not contain any external reference with ultra-voucher', $ex->getMessage()
            );
        }
    }

    /**
     * @throws \Http\Client\Exception
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function testCreateWithFailingOpen()
    {
        $pimple = new \Pimple\Container;
        $dmoId = Uuid::uuid1()->toString();
        $pimple[IDmo::class] = function () use ($dmoId) {
            $dmo = \Mockery::mock(\Giift\Model\IDmo::class);
            $dmo->shouldReceive('getExternalReference')->once()->andReturn('SKU-UV-00050');
            $dmoService = \Mockery::mock(IDmo::class);
            $dmoService->shouldReceive('getFromId')->withArgs([$dmoId])->andReturn($dmo);

            return $dmoService;
        };
        $this->factory->setContainer(new \Pimple\Psr11\Container($pimple));

        $mockApi = \Mockery::mock(Api::class);

        $openOrder = [
            "http_code" => 200,
            "status" => "success",
            "code" => "SSROO1",
            "message" => "Order successfully created",
        ];
        $mockApi->shouldReceive('openOrder')->once()->andReturn($openOrder);

        $this->factory->setUltraVoucherApi($mockApi);

        $orderItem = new CartOrderItem();
        $cartItem = new CartItem();
        $orderItem->setCartItem($cartItem)
            ->setId(Uuid::uuid1()->toString());
        $cartItem->setBuyableType('card')
            ->setBuyableId($dmoId)
            ->setQuantity(42);
        try {
            $this->factory->create($orderItem);
        } catch (\Exception $ex) {
            static::assertStringStartsWith('No invoice_no when opening order ', $ex->getMessage());
        }
    }
}
