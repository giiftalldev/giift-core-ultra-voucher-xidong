<?php

namespace GiiftCore\UltraVoucher;

use Giift\Cart\CartOrderItem;
use Giift\Cart\FactoryResult;
use Giift\Container\Injectable\Container;
use Giift\Http\Injectable\HttpClient;
use Giift\Log\Injectable\Logger;
use Giift\Model\Service\ICard;
use Giift\Model\Service\IDmo;
use Giift\UltraVoucher\Api;
use Giift\UltraVoucher\Utils;
use Ramsey\Uuid\Uuid;

/**
 * Class Factory
 * @package GiiftCore\UltraVoucher
 */
class Factory extends \Giift\Cart\Factory implements \Giift\Cart\IWithAccountBalance
{
    use HttpClient;
    use Logger;
    use Container;

    /** @var null|Api */
    private $api = null;

    private $configLoaded = false;

    /**
     * @return Factory
     */
    private function loadConfig(): Factory
    {
        if (!$this->configLoaded) {
            $this->configLoaded = true;
            \Fuel\Core\Config::load('ultra_voucher', true, true);
        }
        return $this;
    }

    /**
     * @param string $key
     * @return string
     */
    private function getConfig(string $key): string
    {
        $this->loadConfig();
        return \Fuel\Core\Config::get("ultra_voucher.$key");
    }

    /**
     * @param Api $api
     * @return Factory
     */
    public function setUltraVoucherApi(Api $api): Factory
    {
        $this->api = $api;
        return $this;
    }

    /**
     * @return Api
     */
    private function api(): Api
    {
        if (is_null($this->api)) {
            $this->api = (new Api)
                ->setBaseUri($this->getConfig('baseUri'))
                ->setAppId($this->getConfig('appId'))
                ->setAppKey($this->getConfig('appKey'));

            $guzzle = new \GuzzleHttp\Client(['verify' => false]);
            $this->api->setHttpClient(new \Http\Adapter\Guzzle6\Client($guzzle));
        }
        return $this->api;
    }

    /**
     * Create a an object after payment. This does not deliver the object.
     * The object is delivered by the delivery provider.
     *
     * @param  CartOrderItem $orderItem Item to be created.
     *
     * @return FactoryResult
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function create(CartOrderItem $orderItem)
    {
        $requestId = Uuid::uuid1()->toString();
        $orderNumber = $orderItem->getId();
        $receiverName = 'Giift';
        $email = 'ultra-voucher@giift.com';
        $phone = '81290888500';

        $cartItem = $orderItem->getCartItem();
        $dmo = $this->container()->get(IDmo::class)->getFromId($cartItem->getBuyableId());
        if (is_null($dmo)) {
            throw new \Exception("Invalid dmo Id #{$cartItem->getBuyableId()}");
        }
        $sku = $dmo->getExternalReference(Utils::KEY);
        if (is_null($sku)) {
            //@codingStandardsIgnoreLine
            $msg = "Dmo #{$cartItem->getBuyableId()} cannot be bought with ultra-voucher as it does not contain any external reference with ultra-voucher";
            throw new \Exception($msg);
        }

        $openOrder = $this->api()->openOrder(
            $requestId,
            $orderNumber,
            1,
            $sku,
            $receiverName,
            $email,
            $phone
        );

        if (!isset($openOrder['data']) || !isset($openOrder['data']['invoice_no'])) {
            throw new \Exception('No invoice_no when opening order ' . json_encode($openOrder));
        }
        $invoiceNo = $openOrder['data']['invoice_no'];
        $order = $this->api()->getDataOrder(
            $requestId,
            $orderNumber,
            $sku,
            1,
            $invoiceNo,
            1,
            1
        );
        if (!isset($order['data']) || !isset($order['data'][0])) {
            throw new \Exception('No card when fetching data ' . json_encode($order));
        }
        $cardData = $order['data'][0];
        $expiry = new \DateTime($cardData['tgl_expired']);
        $number = base64_decode($cardData['kode_voucher']);
        if (!is_string($number)) {
            throw new \Exception("Card number could not be decoded {$cardData['kode_voucher']}");
        }

        $card = $this->container()->get(ICard::class)->getNew()
            ->setDmo($dmo)
            ->setNumber($number)
            ->setExpiry($expiry)
            ->setValue($cartItem->getQuantity())
            ->setStatus('135024633118xiinl');

        $url = $cardData['url'] ?? null;
        if (is_string($url)) {
            $card->setExtraLogin($url);
        }

        $this->container()->get(ICard::class)->save($card);
        return new FactoryResult(true, $card);
    }

    /**
     * @return float The account balance in the currency from getBalanceCurrency
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function getBalance(): float
    {
        $balance = $this->api()->getBalanceDeposit();
        if (isset($balance['data']) && isset($balance['data']['balance'])) {
            $str = $balance['data']['balance'];
            $str = str_replace(',', '', $str);
            return (float)$str;
        }
        throw new \Exception('Could not fetch balance ' . json_encode($balance));
    }

    /**
     * @return string The balance currency
     */
    public function getBalanceCurrency(): string
    {
        return 'IDR';
    }
}
