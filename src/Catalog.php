<?php

namespace GiiftCore\UltraVoucher;

use Giift\Container\Injectable\Container;
use Giift\Log\Injectable\Logger;
use Giift\Model\IBuyable;
use Giift\Model\ICategory;
use Giift\Model\IDmo;
use Giift\Model\Service\IDmoCategory;
use Giift\Model\Service\ITag;
use Giift\UltraVoucher\Api;
use Intervention\Image\ImageManager;
use League\CLImate\CLImate;

/**
 * Class Catalog
 * @package GiiftCore\UltraVoucher
 */
class Catalog
{
    use Container;
    use Logger;
    /** @var null|Api */
    private $api = null;

    private $configLoaded = false;
    /** @var null|ICategory */
    private $othersCategory = null;

    /** @var null|CLImate */
    private $cli = null;

    /**
     * @return CLImate
     */
    private function cli(): CLImate
    {
        if (is_null($this->cli)) {
            $this->cli = new CLImate();
        }
        return $this->cli;
    }

    /**
     * @return Catalog
     */
    private function loadConfig(): Catalog
    {
        if (!$this->configLoaded) {
            $this->configLoaded = true;
            \Fuel\Core\Config::load('ultra_voucher', true, true);
        }
        return $this;
    }

    /**
     * @param string $key
     * @return string
     */
    private function getConfig(string $key): string
    {
        $this->loadConfig();
        return \Fuel\Core\Config::get("ultra_voucher.$key");
    }

    /**
     * @return Api
     */
    private function api(): Api
    {
        if (is_null($this->api)) {
            $this->api = (new Api)
                ->setBaseUri($this->getConfig('baseUri'))
                ->setAppId($this->getConfig('appId'))
                ->setAppKey($this->getConfig('appKey'));

            $guzzle = new \GuzzleHttp\Client(['verify' => false]);
            $this->api->setHttpClient(new \Http\Adapter\Guzzle6\Client($guzzle));
        }
        return $this->api;
    }

    /**
     * @param \Giift\UltraVoucher\Api $api
     * @return Catalog
     */
    public function setApi(Api $api): Catalog
    {
        $this->api = $api;
        return $this;
    }

    /**
     * @return string
     */
    public function getProviderKey(): string
    {
        return \Giift\UltraVoucher\Utils::KEY;
    }

    /**
     * @return string
     */
    public function getBuyableType(): string
    {
        return 'card';
    }

    /**
     * @return array
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getPrograms(): array
    {
        $brandsResponse = $this->api()->getBrands();

        $brands = [];
        foreach ($brandsResponse['data'] as $brand) {
            $brands[$brand['brand_code']] = $brand;
        }

        $skusResponse = $this->api()->getSkus();
        $skus = [];
        foreach ($skusResponse['data'] as $sku) {
            $skus[$sku['sku']] = $sku;
            $skus[$sku['sku']]['brand'] = $brands[$sku['brand_code']] ?? null;
        }
        return $skus;
    }

//    /**
//     * @param array $program
//     * @return array
//     */
//    private function getRetailer(array $program): array
//    {
//        return $program['brand'];
//    }

    /**
     * @return ICategory|null
     */
    private function getOthersCategory(): ?ICategory
    {
        if (is_null($this->othersCategory)) {
            $this->othersCategory = $this->container()->get(\Giift\Model\Service\ICategory::class)
                ->getOne(['where' => [['cat_name', 'like', '%Others%']]]);
        }
        return $this->othersCategory;
    }

    /**
     * @param IDmo $dmo
     * @param array $program
     * @return IDmo
     */
    private function updateDmo(IDmo $dmo, array $program): IDmo
    {
        try {
            $image = (new ImageManager)->make($program['brand']['image']);
            $dmo->setImage($image);
        } catch (\Throwable $ex) {
            $this->cli()->error($ex->getMessage());
            if (isset($program['brand']['image']) && is_string($program['brand']['image'])) {
                $this->cli()->error($program['brand']['image']);
            }
            // if failed and current dmo does not have anything set to black picture.
            if (empty($dmo->getTemplateUrl())) {
                $image = (new ImageManager)->canvas(180, 120, '#000000');
                $dmo->setImage($image);
            }
        }

        $dmo = $dmo
            ->setName($program['brand']['name'])
            ->setType('xcard')
            ->setCountry('ID')
            ->setLink('https://ultravoucher.co.id/')
            ->setGiiftIssued(false)
            ->setUnit('idr')
            ->setTrackValueProvider('manual')
            ->setInfo('expire', true);

        if (empty($dmo->getCategories())) {
            $cat = $this->getOthersCategory();
            if (!is_null($cat)) {
                $dmoCategoryService = $this->container()->get(IDmoCategory::class);
                $dmoCat = $dmoCategoryService->getNew()
                    ->setDmo($dmo)
                    ->setCategory($cat);
                $dmoCategoryService->save($dmoCat);
            }
        }
        $tagService = $this->container()->get(ITag::class);
        $tags = $tagService->getList(['where' => [['type_id', $dmo->getId()]]]);
        if (empty($tags) && !empty($program['brand']['name'])) {
            $tag = $tagService->getNew()
                ->setDmo($dmo)
                ->setName($program['brand']['name'])
                ->setDisplayName($program['brand']['name'])
                ->setTag($program['brand']['name']);
            $tagService->save($tag);
        }

        return $dmo;
    }

    /**
     * @param IBuyable $buyable
     * @param array $program
     * @return IBuyable
     */
    private function updateBuyable(IBuyable $buyable, array $program): IBuyable
    {
        $value = floatval($program['nominal']);
        $buyable->setInfo(
            'idr',
            [
                'amount' => [
                    'values' => [
                        $value
                    ],
                    'min' => $value,
                    'max' => $value,
                    'default' => $value,
                    "no_custom" => true

                ]
            ]
        );
        return $buyable;
    }

    /**
     * @param array $program
     * @return array
     */
    private function getTnc(array $program): array
    {
        $tncs = ['id' => $program['brand']['description']];
        return array_filter($tncs, function ($tnc) {
            return !empty($tnc);
        });
    }

    /**
     * @param bool $createDmo
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function syncAll(bool $createDmo = true)
    {
        $dmoService = $this->container()->get(\Giift\Model\Service\IDmo::class);
        $buyableService = $this->container()->get(\Giift\Model\Service\IBuyable::class);
        $items = $this->getPrograms();
        $this->logger()->info($this->getProviderKey(), $items);
        foreach ($items as $k => $v) {
            $dmo = $dmoService->getFromExternalReference($this->getProviderKey(), $k);
            if (is_null($dmo) && $createDmo) {
                $dmo = $dmoService->getNew()
                    ->setStatus('136799890089lgdwa')//suspended
                    ->setExternalReference($this->getProviderKey(), $k);
                // We don't want to update the dmos, buyable and tncs.
                // New synchronisation capability will offer more flexibility in the updates.
                // Uncomment next line (bracket) to go back to original
                //}

                //if (is_null($dmo)) {
                //    continue;
                //}

                $this->updateDmo($dmo, $v);

                $dmoService->save($dmo);
                $this->cli()->info($this->getProviderKey() . ' - Updated dmo - ' . $dmo->getName());

                //Buyable
                $buyable = $this->getBuyableType() === 'topup' ? $dmo->getTopupBuyable() : $dmo->getCardBuyable();
                if (is_null($buyable)) {
                    $buyable = $buyableService
                        ->getNew()
                        ->setActive(false)
                        ->setTypeId($dmo->getId())
                        ->setFactoryName($this->getProviderKey())
                        ->setType($this->getBuyableType());
                }

                $buyable = $this->updateBuyable($buyable, $v);
                $buyableService->save($buyable);

                $tncs = $this->getTnc($v);
                foreach ($tncs as $local => $tnc) {
                    $buyable->setTnc($local, $tnc);
                }
            }
        }
    }
}
