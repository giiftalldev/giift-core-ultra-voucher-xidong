<?php
use Giift\UltraVoucher\Utils;
use GiiftCore\UltraVoucher\Factory;

function uv_init()
{
    \Giift_Cart_Buyable_Card::registerFactory(
        Utils::KEY,
        Factory::class,
        'Ultra-Voucher'
    );
}

$_ENV['giift']['uv'] = 'uv_init';
